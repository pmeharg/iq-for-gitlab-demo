# Demo Instructions
## Requirements
* Docker
* Docker Compose
* GitHub access to the demo repo (see step 1)
* A GitHub access token with the `repo` scope
* A GitHub Actions token for the demo repo (see step 1)

## Steps
### Initial One-time Setup
1. Fork https://github.com/sonatype/iq-for-github-demo to your personal GitHub account. Each person needs to have their own fork so as to not interfere with others.
1. Clone your fork
   ```
   git clone https://github.com/<your-github-account>/iq-for-github-demo
   ```
   Note the repo in the `sonatype` organization does have a branch and PR already. The branch comes over in the fork, the PR does not. The setup scripts below will delete the branch from your fork.

1. Change into the demo folder
   ```
   cd iq-for-github-demo
   ```
1. Run the one-time initial setup script.
   ```
   src/setup/one-time-initial-setup-part-1.sh
   ```
   This script will:
   * Run `docker-compose up --build` to start up two containers
   * The first is an IQ instance with the feature flag enabled for Automated Pull Requests (`-DenableScmNotification=true`)
   * The second is a GitHub actions self-hosted runner which will build the project
   * It will also make a Docker volume for the `sonatype-work` folder and store the IQ license there so it survives restarts.
   
   **Note:** The first run of this script will fail as it requires some configuration variables including GitHub access tokens which need to remain private. The script will write out a sample config for you to `github-config`. Simply edit this file and enter your proper values. Instructions are in the file.
   
   Mine looks like (tokens redacted):
   ```
   # You can create an access token here: https://github.com/settings/tokens. All it needs is the 'repo' scope.
   export GITHUB_ACCESS_TOKEN="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
   # You can get your action runner token by going to Settings, Actions, Add runner and finding the token in the installation script
   export GITHUB_ACTION_RUNNER_TOKEN="bbbbbbbbbbbbbbbbbbbbbbbbb"
   export GITHUB_ACCOUNT="collinpeters"
   ```
1. Verify that the GitHub Action self-hosted running came up ok. Navigate to https://github.com/<your-github-account>/iq-for-github-demo/settings/actions and make sure you see a Linux host that is green and 'Idle'.

   If nothing is coming up, check the output of your Docker container with 
   ```
   docker logs iq-for-github-demo_github-actions-runnner_1
   ```
1. Once IQ starts, goto http://localhost:8070 in your browser, login with the default credentials, and install the IQ license. I use '2019-sonatype-internal-nxrm-firewall-lifecycle.lic' from [Confluence](https://docs.sonatype.com/pages/viewpage.action?spaceKey=ProdMgmt&title=Product+Licensing).
As long as the Docker Compose setup isn't torn down, the IQ license install should only have to be done once.
1. Once you have verified things are running properly then (as the output of the previous script suggests), next run the one-time IQ setup script.
   ```
   src/setup/one-time-initial-setup-part-2.sh
   ```
   This script will use the IQ API to:
   * Create a 'Demo Organization'
   * Create the source control config for the organization. This includes setting the default source provider to GitHub, and defaulting to the 'master' branch for pull requests.
   * Delete the 'Architecture-Quality' IQ policy. This policy prevents the remediation API from returning a match for `jackson-databind` since it is so new.
   * Change the default password to `password`. This is to get rid of the banner. **Remember to login with this new password!**
1. An updated iq-server.yml workflow file will be commited to your GitHub repostiory too initiate the first IQ for GitHub Actions policy evaluation. Each subsequent commit will cause a policy evaluation.

## Doing Demos
### Demo Process
The initial setup process above will leave you with:
* A clean IQ report
* A successful GitHub Actions CI build
* An NVS email

Each demo would look something like:
1. Showing that the IQ app has clean policy report
1. Push a commit to uncomment `jackson-databind` in the pom.xml
1. The GitHub CI action will automatically be triggered and do a policy evaluation
1. Show the policy violation in IQ on `jackson-databind` 2.9.9.3
1. A pull request will automatically be generated to remediate this violation
1. Merge the pull request
1. This will trigger a CI build and policy re-evaluation and the violation will disappear from IQ!
1. Each CI build will also send an NVS email

### Resetting for the next demo
For each demo, repeat the steps in this section.
1. Run the `restart-demo.sh` script
   ```
   src/setup/restart-demo.sh
   ```
   
   This script will:
   * Delete and re-create a new 'Demo App' under the 'Demo Organization'
     * Note that when the application is deleted, it takes the policy and source control data with it, allowing for a clean next demo.
   * Configure a new source control record for the re-created app your fork of this repository
   * Delete the branch from the previous test run from your repository on github.com
     * Note that deleting the branch automatically closes the PR as well. If you have merged it and deleted it yourself then that is fine as well.
   * Reset the repository with a force push to revert to jackson-databind commented out
     
1. Note that the force push noted previously will trigger a new GitHub action CI build which will result in a clean IQ policy evaluation without jackson-databind.

## Notes/FAQ
1. Messed up? No problem, run the tear-down script. This script will destroy everything, including the volumes. Start from scratch above. Note the IQ license will have to be installed again.
   ```
   src/setup/tear-it-all-down.sh
   ```
1. The GitHub runner token can get reset automatically. Check the logs of the runner (above) and update your `github-config` if necessary
