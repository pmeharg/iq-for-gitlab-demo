#!/bin/bash

set -e

if [ ! -f "github-config" ]; then
    echo "# You can create an access token here: https://github.com/settings/tokens. All it needs is the 'repo' scope." > github-config
    echo "export GITHUB_ACCESS_TOKEN=\"put-your-token-here\"" >> github-config
    echo "export GITHUB_ACTION_RUNNER_TOKEN=\"put-your-github-action-token-here\"" >> github-config
    echo "export GITHUB_ACCOUNT=\"put-your-github-account-name-here\"" >> github-config

    echo "Error: configuration file not found. Wrote example to $(pwd)/github-config. Please edit this file and try again."
    exit 1
fi

source $(pwd)/github-config

if [ -z "${GITHUB_ACCESS_TOKEN}" ]; then
    echo "Error 'GITHUB_ACCESS_TOKEN' not set. Check your github-config file."
    exit 1
fi
if [ -z "${GITHUB_ACTION_RUNNER_TOKEN}" ]; then
    echo "Error 'GITHUB_ACTION_RUNNER_TOKEN' not set. Check your github-config file."
    exit 1
fi
if [ -z "${GITHUB_ACCOUNT}" ]; then
    echo "Error 'GITHUB_ACCOUNT' not set. Check your github-config file."
    exit 1
fi

docker-compose up -d --build

echo
echo "=== One Time Initial Setup Part 1 is Complete ==="
echo "IQ should be coming up at http://localhost:8070"
echo "Before continuing to part 2:"
echo " * Manually install the IQ license (instructions in readme.md)"
echo " * Verify the GitHub self-hosted runner is fully operational (instructions in readme.md)"
echo "After verifying things are operational, run 'src/setup/one-time-initial-setup-part-2.sh'"
